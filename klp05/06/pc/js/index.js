var opt = {
	speed:400,
	easing:"easeInOutQuint"
}

var controller = new ScrollMagic.Controller({globalSceneOptions: {duration:0 , offset:-200}});

$(function(){	
	
	// globalmenu fixed gnav
	var offset = $('#gnavi').offset();
    $(window).scroll(function () {
        if ($(window).scrollTop() > offset.top) {
			$('#gnavi').addClass('fixed');
			//$(".gnavi_block").css("display","block");
        } else {
			$('#gnavi').removeClass('fixed');
			//$(".gnavi_block").css("display","none");
        }
    });
	
	// globalmenu gnav
	$('#nav_toggle').click(function(){
		$("#gnavi").toggleClass('open');
		$("nav").slideToggle(500);
	});
	//menu link click gnav
	$('nav a').click(function(){
		//if(window.innerWidth <= 680){
			$("#gnavi").toggleClass('open');
			$("nav").slideToggle(500);
		//}
	});	
	
			// globalmenu fixed gtell
	var offset = $('#gtell').offset();
    $(window).scroll(function () {
        if ($(window).scrollTop() > offset.top) {
			$('#gtell').addClass('fixed');
			//$(".gnavi_block").css("display","block");
        } else {
			$('#gtell').removeClass('fixed');
			//$(".gnavi_block").css("display","none");
        }
    });
	
	// globalmenu gtell
	$('#gtell_toggle').click(function(){
		$("#gtell").toggleClass('open');
		$(".gtell").slideToggle(500);
		$("#gnavi").toggleClass('none');
		
	});
	//menu link click gtell
	$('.gtell a').click(function(){
		//if(window.innerWidth <= 680){
			$("#gtell").toggleClass('open');
			$(".gtell").slideToggle(500);	
		   $("#gnavi").toggleClass('none');
		//}
	});	
	
	// globalmenu gtell
	$('#gtell_toggle-t').click(function(){
		$("#gtell-t").toggleClass('open');
		$(".gtell-t").slideToggle(500);
		
	});
	//menu link click gtell
	$('.gtell-t a').click(function(){
		//if(window.innerWidth <= 680){
			$("#gtell-t").toggleClass('open');
			$(".gtell-t").slideToggle(500);	
		//}
	});	

	
	
	const controller = new ScrollMagic.Controller();
	//アニメーション
	$(".motion-animate").each(function(){
		let s = $(this).offset().top - 300;
		let d = $(this);
		new ScrollMagic.Scene({
			triggerElement:'body',
			offset: s
		})
		.on('start', function(){
			d.addClass("active");
		})
		//.addIndicators()
		.addTo(controller);
		
	});	
	
	new ScrollMagic.Scene({triggerElement: '.second h2'}).on('start', function(){
			$(".second li figure").addClass("active");
	}).addTo(controller);
	
	$("#FIRSTVIEW strong").textillate({
			loop: false,
			minDisplayTime: 3000,
			initialDelay: 0,
			autoStart: true,
			in:{
				effect: 'fadeIn',
				delayScale: 1.5,
				delay: 80,
				sync: false,
				shuffle: true
			}					
		});

	$(".fistview_item").addClass("move");

$(document).ready(function(){
var hSize = $(window).height();
  $('#FIRSTVIEW').height(hSize);
});
$(window).resize(function(){
var hSize = $(window).height();
  $('#FIRSTVIEW').height(hSize);
});


$('#gnavi nav li a,#gtell .gtell li a').on("click", function(){ 
        var speed = 500;
        var href= $(this).attr("href"); 
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top - 85;
        $("html, body").animate({scrollTop:position}, speed, "swing");
        return false;
    });

	
$('#spmore-toggle').on("click" , function() {
         if (window.matchMedia('(max-width: 768px)').matches) {
		$(".spmore-li").toggleClass('open');
			 $("#spmore-toggle").toggleClass('open');
         } else if (window.matchMedia('(min-width:768px)').matches) {
	;
}
          });

});



