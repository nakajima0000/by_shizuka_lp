$('.accordionContent').hide();
$('.accordionTrigger').on('click', function () {
  $(this).fadeOut();
  $('.accordionContent').fadeIn();
});

var scroll = new SmoothScroll('a[href*="#"]');

$(window).on('load, scroll', function () {
  var $btn = $('#button01');
  var scrollHeight = $(document).height();
  var scrollPosition = $(window).height() + $(window).scrollTop(); //現在地 
  var targetHeight = $('#offer').innerHeight();
  if (scrollHeight - scrollPosition <= targetHeight) {
    $btn.fadeOut();
  } else {
    $btn.fadeIn();
  }
});