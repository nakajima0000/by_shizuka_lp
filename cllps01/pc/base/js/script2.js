$(function() {
  // Inview
  $('.inview').on('inview', function(e, isInview) {
    if (isInview) {
      $(this).addClass('is-inview');
    } else {
      //$(this).removeClass('is-inview');
    }
  });

  // Accordion
  $('.sec17 .acc-btn').on('click', function(){
    $this = $(this);
    $acc = $this.parent();
    $this.prev().slideToggle(function() {
      $acc.toggleClass('is-active');
    });
  });
  $('.sec18 .acc dt').on('click', function() {
    $this = $(this);
    $this.next().slideToggle(function() {
      $this.toggleClass('is-active');
    });
  })
});
