$(function() {
  // Inview
  var $form = $('.form'),
      clsInview = 'is-inview',
      y,
      isForm = false;
  $('.inview').on('inview', function(e, isInview) {
    $this = $(this);
    isForm = $this.hasClass('form');
    if (isInview) {
      $this.addClass(clsInview);
    } else {
      if (isForm) {
        if (y - $form.offset().top < 0) {
          $this.removeClass('is-inview');
        }
      }
    }
  });
  $(window).on('scroll', function(){
    y = $(this).scrollTop();
    if (y < 100) {
      $form.addClass(clsInview);
      isForm = false;
    } else if (!isForm) {
      $form.removeClass(clsInview);
    }
  });
  $form.addClass(clsInview);

  // Accordion
  $('.sec17 .acc-btn').on('click', function(){
    $this = $(this);
    $acc = $this.parent();
    $this.prev().slideToggle(function() {
      $acc.toggleClass('is-active');
    });
  });
  $('.sec18 .acc dt').on('click', function() {
    $this = $(this);
    $this.next().slideToggle(function() {
      $this.toggleClass('is-active');
    });
  })
});
