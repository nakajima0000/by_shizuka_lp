$(function() {
  // Inview
  var y;
  $('.inview').on('inview', function(e, isInview) {
    $this = $(this);
    if (isInview) {
      $this.addClass('is-inview');
    } else {
      if ($this.hasClass('form')) {
        if (y - $this.offset().top < 0) {
          $this.removeClass('is-inview');
        }
      }
    }
  });
  $(window).scroll(function(){
    y = $(this).scrollTop();
	});

  // Accordion
  $('.sec17 .acc-btn').on('click', function(){
    $this = $(this);
    $acc = $this.parent();
    $this.prev().slideToggle(function() {
      $acc.toggleClass('is-active');
    });
  });
  $('.sec18 .acc dt').on('click', function() {
    $this = $(this);
    $this.next().slideToggle(function() {
      $this.toggleClass('is-active');
    });
  })
});
