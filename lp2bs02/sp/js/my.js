function smartRollover() {
	if(document.getElementsByTagName) {
		var images = document.getElementsByTagName("img");
		for(var i=0; i < images.length; i++) {
			if(images[i].getAttribute("src").match("_off."))
			{
				images[i].onmouseover = function() {
					this.setAttribute("src", this.getAttribute("src").replace("_off.", "_on."));
				}
				images[i].onmouseout = function() {
					this.setAttribute("src", this.getAttribute("src").replace("_on.", "_off."));
				}
			}
		}
	}
}
if(window.addEventListener) {
	window.addEventListener("load", smartRollover, false);
}
else if(window.attachEvent) {
	window.attachEvent("onload", smartRollover);
}
//ページ内スクロール
$(function(){
	$('a[href^=#]').on('click', function(){
		//クリックした要素の#を変数に入れる
		var Target = $(this.hash);
		//行き先までの画面上の高さの数値を変数に入れる
		var TargetOffset = $(Target).offset().top;
		//アニメーション時間ミリ秒
		var Time = 700;
		//集めた情報を元にアニメーション
		$('html, body').animate({
			scrollTop: TargetOffset
		}, Time);
		return false;
	});
});



//アコーディオン・モーダルボタン
	$(function() {
		$(".accordion").click(function(){
				$(this).next("dd").slideToggle();
				$(this).toggleClass("open");//.children(".ac2")
		});
		$(".more_link").click(function(){
				$(".area_more").slideToggle();
				$(this).toggleClass("open");//.children(".ac2")
		});
		$(".next").click(function(){
				$(this).next(".next_txt").slideToggle();
				$(this).toggleClass("open");//.children(".ac2")
		});

	$(window).scroll(function(){
		var y = $(this).scrollTop();
		if((y >= ($(".com").offset().top)) && (y < ($(".end").offset().top) - $(window).height())){
				$(".contactWrap02").fadeIn('fast');          //fadeIn
		};
		 if(y < ($(".com").offset().top)){
				$(".contactWrap02").fadeOut('fast');          //fadeOut
		};
		if(y >= ($(".end").offset().top) - $(window).height()){
				$(".contactWrap02").fadeOut('fast');          //fadeOut
		};
		});


	});


