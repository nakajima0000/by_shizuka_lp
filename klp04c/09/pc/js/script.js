var scroll = new SmoothScroll('a[href*="#"]');

$(window).on('load, scroll', function () {
  var $btn = $('#button01');
  var scrollHeight = $(document).height();
  var scrollPosition = $(window).height() + $(window).scrollTop(); //現在地 
  var targetHeight = $('#offer').innerHeight();
  if (scrollHeight - scrollPosition <= targetHeight) {
    $btn.fadeOut();
  } else {
    $btn.fadeIn();
  }
});

$('.inview').on('inview', function (event, isInView) {
  if (isInView) {
    $(this).addClass('is-inview');
  } else {
    // element has gone out of viewport
  }
});

//rollover
$('img').hover(
  function () {
    $(this).attr('src', $(this).attr('src').replace('_off', '_on'));
  },
  function () {
    if (!$(this).hasClass('cur')) {
      $(this).attr('src', $(this).attr('src').replace('_on', '_off'));
    }
  }
);